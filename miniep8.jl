

using Test

# Parte 1

function Cards(a)
    if a == "J"
        return 11
    elseif a == "Q"
        return 12
    elseif a == "K"
        return 13
    elseif a == "A"
        return 14
    else
        return a 
    end
end


function CompareByValue(x, y)
    x = Cards(x)
    y = Cards(y)

    if x < y
        return true
    else
        return false
    end
end

function test()
    @test CompareByValue(3, 6)
    @test !CompareByValue(7, 3)
    @test CompareByValue(4, "A")
    @test CompareByValue(7, "J")
    @test CompareByValue("K", "A")
    @test !CompareByValue("Q", "J")

    println("Fim dos testes")
end

# test()

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

 function insercao1(v)
    tam = length(v)
    for i in 2:tam
        j = i 
        while j > 1
            if CompareByValue(v[j], v[j-1])
                troca(v, j, j-1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
    
end

 function test_insercao1()
    @test insercao([1, "J", 10, "Q", 5, "A"]) == [1, 5, 10, "J", "Q", "A"]
    println("Fim dos testes")
end

 # test_insercao()

# Parte 2

function CardsWithSuits(a)
    cards = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
    tam = length(cards)
    u = []

    for i in 1:tam
           push!(u, cards[i]*"♢")
    end
     
    for i in 1:tam
        push!(u, cards[i]*"♠")
    end

    for i in 1:tam
        push!(u, cards[i]*"♡")
    end

    for i in 1:tam
        push!(u, cards[i]*"♣")
    end

    for i in 1:56
        if a == u[i]
            return i
        end
    end
end
   
 
function CompareByValueAndSuit(x, y)

    x = CardsWithSuits(x)
    y = CardsWithSuits(y)

    if x < y
        return true
    else
        return false
    end
end

function test_1()
    @test CompareByValueAndSuit("2♣", "A♣")
    @test !CompareByValueAndSuit("K♠", "10♠")
    @test CompareByValueAndSuit("10♢", "10♠")
    @test CompareByValueAndSuit("A♠", "2♣")
    println("Final dos Testes")
end


# test_1()

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i 
        while j > 1
            if CompareByValueAndSuit(v[j], v[j-1])
                troca(v, j, j-1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function test_insercao()
    @test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♢", "J♠", "K♠", "A♠", "A♠", "10♡"]
    println("Final dos testes")
end

 # test_insercao()
    